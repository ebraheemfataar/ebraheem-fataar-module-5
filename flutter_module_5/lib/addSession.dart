import 'dart:developer';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:flutter_module_5/sessionList.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addSessions() {
      final subject = subjectController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("sessions").doc();

      return ref
          .set(
              {"Subject Name": subject, "location": location, "dpc_id": ref.id})
          .then((value) => log("Collection added!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: subjectController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: 'Enter Subject Name'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: 'Enter Location'),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  _addSessions();
                },
                child: Text("Add Session"))
          ],
        ),
        SessionList()
      ],
      
    );
  }
}
